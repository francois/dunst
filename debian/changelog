dunst (1.3.2-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/copyright: Use https protocol in Format field
  * d/rules: Remove trailing whitespaces

  [ Michael Stapelberg ]
  * delete gbp.conf to get rid of pristine-tar
  * New upstream version 1.3.2
  * drop debian/patches/makefile.patch (applied upstream)
  * debian/patches/cross.patch: make pkg-config substitutable
  * debian/rules: supply $PKG_CONFIG for all targets
    (Closes: #910984)

 -- Michael Stapelberg <stapelberg@debian.org>  Sun, 14 Oct 2018 20:16:04 +0200

dunst (1.3.0-3) unstable; urgency=medium

  [ Pascal De Vuyst ]
  * depend on default-dbus-session-bus | dbus-session-bus (Closes: #859453)

 -- Michael Stapelberg <stapelberg@debian.org>  Sun, 05 Aug 2018 19:27:16 +0200

dunst (1.3.0-2) unstable; urgency=medium

  * Makefile: correct dependencies to fix race condition (Closes: #888760)

 -- Michael Stapelberg <stapelberg@debian.org>  Mon, 29 Jan 2018 16:53:39 +0100

dunst (1.3.0-1) unstable; urgency=medium

  * Enable pristine-tar in debian/gbp.conf to reflect status quo
  * Update Vcs-* tags after moving to salsa
  * New upstream version 1.3.0
  * refresh patches
  * add build-dep on systemd so that dunst installs the service file
  * priority extra was replaced by priority optional

 -- Michael Stapelberg <stapelberg@debian.org>  Sun, 28 Jan 2018 09:50:17 +0100

dunst (1.2.0-2) unstable; urgency=medium

  * rm debian/org.knopwob.dunst.service to not shadow upstream (Closes: #878164)

 -- Michael Stapelberg <stapelberg@debian.org>  Tue, 10 Oct 2017 22:28:17 +0200

dunst (1.2.0-1) unstable; urgency=medium

  * Update upstream URLs
  * New upstream version 1.2.0
  * Refresh patch
  * Update Build-Deps
  * Use a secure Vcs URI
  * switch to debhelper compat 10
  * Enable all hardening
  * Bump Standards-Version to 4.1.1 (no changes necessary)

 -- Michael Stapelberg <stapelberg@debian.org>  Thu, 05 Oct 2017 21:30:51 +0200

dunst (1.1.0-2) unstable; urgency=medium

  * Install dunstrc to /etc/xdg/dunst/dunstrc (Closes: #765827)
  * Update debian/copyright: draw.* and ini.* have been removed in previous
    new upstream releases.
  * s/BSD/BSD-3-clause/ in debian/copyright
  * Bump Standards-Version to 3.9.6 (no changes necessary)

 -- Michael Stapelberg <stapelberg@debian.org>  Fri, 07 Nov 2014 09:59:00 +0100

dunst (1.1.0-1) unstable; urgency=medium

  * New upstream release.
    Closes: #740840, #705607, #750596, #729690

 -- Michael Stapelberg <stapelberg@debian.org>  Thu, 31 Jul 2014 10:04:45 +0200

dunst (1.0.0-2) unstable; urgency=low

  * experimental to unstable because dunst 1.0.0 was only in experimental due
    to the freeze. I even got a user request to provide 1.0.0 in !experimental
    because it supports RTL text.
  * Canonicalize Vcs-* fields

 -- Michael Stapelberg <stapelberg@debian.org>  Sun, 16 Jun 2013 23:30:14 +0200

dunst (1.0.0-1) experimental; urgency=low

  * New upstream version
    (Configured fonts might break due to the switch to pango)
  * Bump standards-version to 3.9.4 (no changes necessary)

 -- Michael Stapelberg <stapelberg@debian.org>  Mon, 15 Apr 2013 15:18:58 +0200

dunst (0.5.0-1) experimental; urgency=low

  * New upstream version

 -- Michael Stapelberg <stapelberg@debian.org>  Wed, 30 Jan 2013 21:30:19 +0100

dunst (0.4.0-1) experimental; urgency=low

  * New upstream version

 -- Michael Stapelberg <stapelberg@debian.org>  Thu, 27 Sep 2012 12:55:06 +0200

dunst (0.3.1-1) experimental; urgency=low

  * Imported Upstream version 0.3.1
  * move packaging to collab-maint
  * update patches/example_path.patch for 0.3.1
  * enable hardening
  * Make Makefile use overwritten variables like CFLAGS (for hardening)

 -- Michael Stapelberg <stapelberg@debian.org>  Wed, 08 Aug 2012 22:26:27 +0200

dunst (0.2.0-3) unstable; urgency=low

  * Ship org.knopwob.dunst.service so that dunst gets D-Bus-activated when a
    notification arrives. Thanks Jakob Haufe. (Closes: #683089)

 -- Michael Stapelberg <stapelberg@debian.org>  Thu, 02 Aug 2012 00:07:31 +0200

dunst (0.2.0-2) unstable; urgency=low

  * Add Provides: notification-daemon so that you can use dunst to replace
    another notification-daemon. Thanks Andrew Hout. (Closes: #681732)

 -- Michael Stapelberg <stapelberg@debian.org>  Mon, 16 Jul 2012 20:31:29 +0200

dunst (0.2.0-1) unstable; urgency=low

  * Initial release (Closes: #678445)

 -- Michael Stapelberg <stapelberg@debian.org>  Wed, 27 Jun 2012 18:02:29 +0200
