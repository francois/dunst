Source: dunst
Section: x11
Priority: optional
Maintainer: Michael Stapelberg <stapelberg@debian.org>
Build-Depends: debhelper (>= 10),
               libdbus-1-dev,
	       libx11-dev,
	       libxinerama-dev,
	       libxss-dev,
	       libxdg-basedir-dev,
	       libglib2.0-dev (>= 2.36),
	       libpango1.0-dev,
	       libcairo2-dev,
	       libnotify-dev,
	       libxrandr-dev,
	       libgtk2.0-dev,
	       dpkg-dev (>= 1.16.1.1),
	       systemd
Standards-Version: 4.1.1
Homepage: https://dunst-project.org/
Vcs-Git: https://salsa.debian.org/debian/dunst.git
Vcs-Browser: https://salsa.debian.org/debian/dunst

Package: dunst
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, default-dbus-session-bus | dbus-session-bus
Provides: notification-daemon
Description: dmenu-ish notification-daemon
 Dunst is a highly configurable and lightweight notification-daemon: The
 only thing it displays is a colored box with unformatted text. The whole
 notification specification (non-optional parts and the "body" capability) is
 supported as long as it fits into this look & feel.
 .
 Dunst is designed to fit nicely into minimalistic windowmanagers like dwm, but
 it should work on any Linux desktop.
